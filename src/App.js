import React from 'react';
import './App.css';

import startRequest from './services/startRequest';

import { ProductItem } from './components/ProductItem';
import { SocialNetworkIcon } from './components/SocialNetworkIcon';
import { MobileNavBlock } from './components/MobileNavBlock';
import { LogoBlock } from './components/LogoBlock';
import { LanguageBlock } from './components/LanguageBlock';
import { BasketBlock } from './components/BasketBlock';
import { Main } from './components/Main';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.changeBasketCounter = this.changeBasketCounter.bind(this);
    this.state = {
      basketCounter: 0, 
      basket: [],
      productData: null, 
      socialNetworksData: null,
    };
  }

  componentDidMount() {
    let closureThis = this;
    startRequest({route: './sources/socialNetworkIcons.json'})
      .then(data => {
        closureThis.setState({socialNetworksData: (data.ok) ? data.body : null});
      });

    startRequest({route: '/sources/productData.json'})
      .then(data => {
        closureThis.setState({productData: (data.ok) ? data.body : null});
      });
  }

  changeBasketCounter(id) {
    let basket = this.state.basket;
    if (!basket.includes(id)) {
      basket.push(id);
      this.setState({basketCounter: ++this.state.basketCounter, basket: basket});
      localStorage.setItem('basket', basket);
    }

  }

  render() {
    const productItems = this.state.productData;
    let listProductItems = null;
    if (productItems) {
      listProductItems = productItems.map((item) =>
        <ProductItem key={item.id} data={item} handleClick={this.changeBasketCounter}/>
      );
    }

    const socialNetworks = this.state.socialNetworksData;
    let socialNetworksList = null;
    if (socialNetworks) {
      socialNetworksList = socialNetworks.map((item) => 
        <SocialNetworkIcon key={item.name} data={item}/>
      );
    }

    return (
      <div className="container">
        <div className="mask">
          <header>
            <MobileNavBlock/>
            <div className="centerBlock">
              <LogoBlock/>
              <LanguageBlock/>
            </div>
            <BasketBlock basketCounter={this.state.basketCounter}/>
          </header>
          <Main/>
          <footer>
            <ul className="footerIconList">
              {socialNetworksList}
            </ul>
          </footer>
        </div>
        <ul className="productList">
          {listProductItems}
        </ul>
      </div>
    );
  }
}

export default App;
