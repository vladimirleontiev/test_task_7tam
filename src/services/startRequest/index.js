const SERVER = window.location.protocol + '//' + window.location.host;
// console.log(SERVER);

export default function startRequest(data) {

  if (data.route.substr(0, 2) == './') {
    // getting resources from a project (relative addressing)
    return fetch(data.route)
      .then(source => source.json())
      .then(source => {
        return { ok: true, status: 200, body: source }
      });
  } else {
    // response delay emulation
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(data);
      }, 1500);
    })
    .then((data) => {
      // request for backend
      /*
       body - body to POST;
       additionalHeaders format: [[key, value], [key, value], [key, value]...]
      */
      let {route, body, token, additionalHeaders} = data;

      if (body) {
        let headers = {};

        let addHeaders = new GetHeaders(token);
        if (additionalHeaders && Array.isArray(additionalHeaders)) {
          addHeaders.concat(additionalHeaders);
        }
        for (let i = 0; i < addHeaders.length; i++) {
          headers['' + addHeaders[i][0]] = '' + addHeaders[i][1];
        }
    
        return fetch(SERVER + route, {
          method: "POST",
          headers: headers,
          body: body
        })
        .then(response => getResponseObj(response))
        .then(responseObj => responseObj);
        // correctly emulate the delay here, but it is more convenient at the beginning
      } else {
        return fetch(SERVER + route)
        .then(response => getResponseObj(response))
        .then(responseObj => responseObj);
        // correctly emulate the delay here, but it is more convenient at the beginning
      }
    });
  }


  function GetHeaders(token) {
    let output = [];
    output.push(["Content-Type", "application/json;charset=utf-8"]);
    if (token) {
                                    /* some kind of token format */
      output.push(["Authorization", token]);
    }
    return output;
  }

  async function getResponseObj(response) {
    let body;
    try {
      body = await response.json();
    } catch (e) {
      body = null;
    }
    return { ok: response.ok, status: +response.status, body: body };
  }
};