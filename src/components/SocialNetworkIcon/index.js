import React from 'react';
import './styles.css';

import { Points } from '../Points';

export class SocialNetworkIcon extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <li className="socialNetworkIcon">
        <Points/>
        <a href={this.props.data.linkPath} target="_blank">
          <img src={this.props.data.iconPath} alt={this.props.data.name}></img>
        </a>
      </li>
    );
  }
}