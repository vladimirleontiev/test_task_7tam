import React from 'react';
import './styles.css';

import { SortByButton } from '../SortByButton';
import { BannerBlock } from '../BannerBlock';

export class Main extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <main>
        <section className="left-part">
          <div className="wraper">
            <h2>sort by</h2>
            <ul className="sortByBlock">
              <SortByButton text="by manufacturer"/>
              <SortByButton text="minimum price"/>
              <SortByButton text="maximum price"/>
            </ul>
          </div>
        </section>

        <section className="center-part">
          <div className="wraper">
            <h2>on sale</h2>
            <div className="table-header">
              <ul className="table-header__table">
                <li>release</li>
                <li>manufacter</li>
                <li>model</li>
                <li>hash</li>
                <li>algorithm</li>
                <li>efficiency</li>
                <li>profit</li>
                <li>price</li>
              </ul>
            </div>
          </div>
        </section>

        <section className="right-part">
          <div className="wraper">
            <h2>news</h2>
            <BannerBlock/>
          </div>
        </section>
      </main>
    );
  }
}

// export default Main;