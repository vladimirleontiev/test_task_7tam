import React from 'react';
import './styles.css';

export class ProductItem extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    /* this.обработчик события = this.обработчик события.bind(this) */
    /* this.state = {key: value}; */
  }

  handleClick(e) {
    this.props.handleClick(this.props.data.id);
  }

  render() {
    return (
      <li className="productListItem" onClick={this.handleClick}>
        <ul className="desktopContainer">
          <li>
            <img src="img/star.svg"/>
            <img src="img/green_arrow.svg"/>
          </li>
          <li>
            {this.props.data.properties.release.month}
            &ensp;
            {this.props.data.properties.release.year}
          </li>
          <li>{this.props.data.properties.manufacturer}</li>
          <li>{this.props.data.properties.model}</li>
          <li>
            {this.props.data.properties.hash.min}
            -
            {this.props.data.properties.hash.max}
            &thinsp;
            {this.props.data.properties.hash.unit}
          </li>
          <li>{this.props.data.properties.algorithm}</li>
          <li>
            {this.props.data.properties.efficiency.value}
            &thinsp;
            {this.props.data.properties.efficiency.unit}
          </li>
          <li>
            <span className="greenText">
              {this.props.data.properties.profit.currencyIcon}
              {this.props.data.properties.profit.value}
            </span>
            &thinsp;/&thinsp;
            {this.props.data.properties.profit.interval}
          </li>
          <li className="whiteText">
            {this.props.data.properties.price.currencyIcon}
            {this.props.data.properties.price.min}
            &thinsp;-&thinsp;
            {this.props.data.properties.price.currencyIcon}
            {this.props.data.properties.price.max}
          </li>
        </ul>
        <div className="mobileContainer">
          <div className="descriptionPart">
          
          </div>
          <div className="imagePart">
          
          </div>
          <div className="pricePart">
          
          </div>
        </div>
      </li>
    );
  }
}