import React from 'react';
import './styles.css';

import { Points } from '../Points';

export class SortByButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <li className="sortByBlockItem">
        <Points/>
        <button className="sortByBlockButton">
          {this.props.text}
        </button>
      </li>
    );
  }
}