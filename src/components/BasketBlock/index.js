import React from 'react';
import './styles.css';

export class BasketBlock extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="basketBlock">
        <img src="img/basketIcon.svg" alt="basket icon"/>
        <span className="basketCounter">{this.props.basketCounter}</span>
      </div>
    );
  }
}