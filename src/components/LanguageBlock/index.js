import React from 'react';
import './styles.css';

export class LanguageBlock extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <button className="languageBlock">
        <img src="img/languageIcon.svg" alt="language icon"/>
        <span className="languageDescrip">
          eng
        </span>
      </button>
    );
  }
}