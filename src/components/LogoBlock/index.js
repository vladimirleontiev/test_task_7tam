import React from 'react';
import './styles.css';

export class LogoBlock extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <ul className="logoBlock">
        <li><img src="img/logo_C.svg"/></li>
        <li><img src="img/logo_O.svg"/></li>
        <li><img src="img/logo_R.svg"/></li>
        <li><img src="img/logo_E.svg"/></li>
        <li><img src="img/logo_X.svg"/></li>
      </ul>
    );
  }
}