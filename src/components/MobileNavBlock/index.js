import React from 'react';
import './styles.css';

export class MobileNavBlock extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="mobileNavBlock">
        <img src="img/mobileNavIcon.svg" alt="navigation button icon"/>
        <nav className="mobileNavContainer">

        </nav>
      </div>
    );
  }
}