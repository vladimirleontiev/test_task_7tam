import React from 'react';
import './styles.css';

import startRequest from '../../services/startRequest';

import { Checkbox } from '../Checkbox';
import { Points } from '../Points';

export class BannerBlock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {bannerData: null};
  }
  render() {
    const closureThis = this;
    startRequest({route: './sources/bannerData.json'})
      .then(responseObj => {
        closureThis.state.bannerData = (responseObj.ok) ? responseObj.body : null;
      });

    return (this.state.bannerData) ? (
      <div className="bannerBlock">
        <Points/>
        <img src={this.state.bannerData.url} alt={this.state.bannerData.alt}/>
        <ul className="controlBox">
          <li><Checkbox/></li>
          <li><Checkbox/></li>
          <li><Checkbox/></li>
        </ul>
      </div>
    ) : null;
  }
}